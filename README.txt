COMMERCE SUOMENVERKKOMAKSUT NOTIFY README
===================================
This module marks Paytrail payments successful, if the URL
with correct parameters is accessed without a saved shopping session.

REQUIREMENTS
------------
This module works together with commerce suomenverkkomaksut
 * Commerce Payment: Suomen Verkkomaksut
   (https://www.drupal.org/project/commerce_suomenverkkomaksut)
 * Drupal Commerce (https://drupal.org/project/commerce)

INSTALLATION
============
- Enable the module as usual.

CONFIGURATION
-------------
There are no configurations needed from this module,
but you need to configure Return address (notify) in
Commerce Payment: Suomen Verkkomaksut module to
commerce_svm_notify/!order_id/!payment_redirect_key

MAINTAINERS
-----------
Current maintainers:
 * Atte Kilpela (attekilpela) - https://www.drupal.org/user/1159742
 * Kaarel Elissaar (kaarele) - https://www.drupal.org/user/1804962
